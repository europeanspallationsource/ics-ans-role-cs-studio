ics-ans-role-cs-studio
======================

Ansible role to install CS-Studio.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
csstudio_version: 4.5.7.0
csstudio_repository: production
csstudio_base_url: "https://artifactory.esss.lu.se/artifactory/CS-Studio"
csstudio_archive: "{{csstudio_base_url}}/{{csstudio_repository}}/{{csstudio_version}}/cs-studio-ess-{{csstudio_version}}-linux.gtk.x86_64.tar.gz"
csstudio_fonts: https://artifactory.esss.lu.se/artifactory/swi-pkg/fonts/cs-studio-fonts-20180222.tgz
csstudio_fonts_dir: /usr/share/fonts/ess-20180222
csstudio_xulrunner_version: 1.9.2.29pre
csstudio_xulrunner: https://artifactory.esss.lu.se/artifactory/swi-pkg/xulrunner/xulrunner-{{csstudio_xulrunner_version}}.en-US.linux-x86_64.tar.bz2
csstudio_install_python: true

# ":" separated list of addresses
csstudio_ca_addr_list: localhost
# root components defined in beast playbook
csstudio_beast_root_component: MAIN
csstudio_annunciator_jms_topic: ALARM
# Following variables should be identical to the ones set in the beast playbook
beast_rdb_url: jdbc:mysql://localhost/alarm
beast_rdb_user: alarm
beast_rdb_password: alarmpwd
beast_jms_url: failover:(tcp://localhost:61616)
beast_jms_user: admin
beast_jms_password: admin
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-cs-studio
```

License
-------

BSD 2-clause
