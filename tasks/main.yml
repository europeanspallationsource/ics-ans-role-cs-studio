---
- name: install required dependencies
  yum:
    name: "{{item}}"
    state: present
  with_items:
    - bzip2
    # install fonts that have precedence on others to avoid kerning issues
    - urw-fonts

- name: check CS-Studio current version
  command: "cat {{csstudio_version_file}}"
  register: csstudio_current_version
  failed_when: False
  changed_when: False
  check_mode: no

- name: current CS-Studio version
  debug:
    msg: "CS-Studio current version: {{csstudio_current_version.stdout}}"

- name: remove previous backup if it exists
  file:
    path: "{{csstudio_directory}}-{{csstudio_current_version.stdout}}"
    state: absent
  when:
    - csstudio_current_version.stdout != ""
    - csstudio_current_version.stdout != csstudio_version

- name: backup current version
  command: "mv {{csstudio_directory}} {{csstudio_directory}}-{{csstudio_current_version.stdout}}"
  args:
    creates: "{{csstudio_directory}}-{{csstudio_current_version.stdout}}"
  when:
    - csstudio_current_version.stdout != ""
    - csstudio_current_version.stdout != csstudio_version

- name: install CS-Studio
  unarchive:
    src: "{{csstudio_archive}}"
    dest: /opt
    remote_src: yes
    owner: root
    group: root
    creates: "{{csstudio_directory}}"

- name: customize CSS preference settings
  lineinfile:
    state: present
    path: /opt/cs-studio/configuration/plugin_customization.ini
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
  with_items: "{{ csstudio_preferences }}"

- name: customize channel access address list
  lineinfile:
    state: present
    path: /opt/cs-studio/configuration/diirt/datasources/ca/ca.xml
    regexp: '^(\s*addr_list=)'
    line: '\1"{{ csstudio_ca_addr_list }}"'
    backrefs: yes

- name: copy the /usr/local/bin/css script
  copy:
    src: css
    dest: /usr/local/bin/css
    owner: root
    group: root
    mode: 0755

- name: create the /ess directory
  file:
    path: /ess
    state: directory
    owner: root
    group: root
    mode: 0755

- name: add CS-Studio to the desktop menu
  copy:
    src: ESS-CS-Studio.desktop
    dest: /usr/share/applications
    owner: root
    group: root
    mode: 0644

- name: install xulrunner
  unarchive:
    src: "{{ csstudio_xulrunner }}"
    dest: /opt
    remote_src: yes
    owner: root
    group: root

- name: create the /ess/xulrunner link for backward compatibility
  file:
    src: /opt/xulrunner
    dest: /ess/xulrunner
    state: link
    owner: root
    group: root

- name: create the CS-Studio fonts directory
  file:
    path: "{{ csstudio_fonts_dir }}"
    state: directory
    owner: root
    group: root
    mode: 0755

# When changing the fonts, don't forget to remove
# the previous directories!
- name: remove old CS-Studio fonts
  file:
    path: "/usr/share/fonts/{{ item }}"
    state: absent
  with_items:
    - ess

- name: install CS-Studio fonts
  unarchive:
    src: "{{ csstudio_fonts }}"
    dest: "{{ csstudio_fonts_dir }}"
    remote_src: yes
    owner: root
    group: root

- name: install Python environment
  include_role:
    name: ics-ans-role-conda
  vars:
    conda_env_files:
      - csstudio_env.yml
  when: csstudio_install_python
